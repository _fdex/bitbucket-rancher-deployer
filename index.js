const { default: Axios } = require('axios');

const RANCHER_ACCESS = process.env.RANCHER_ACCESS;
const RANCHER_KEY = process.env.RANCHER_KEY;
const RANCHER_URL = process.env.RANCHER_URL;
const PROJECT_ID = process.env.PROJECT_ID;
const STACK_NAME = process.env.STACK_NAME;
const SERVICE_NAME = process.env.SERVICE_NAME;
const DOCKER_IMAGE = process.env.DOCKER_IMAGE;

const handleError = (err) => {
    console.log(err);
    process.exit(1)
};

const auth = {
    username: RANCHER_ACCESS,
    password: RANCHER_KEY
};

const axios = Axios.create({
    baseURL: `${RANCHER_URL}/v2-beta/projects`,
    auth,
    timeout: 30000
})

const makeCall = async (url, method, data) => {

    try {
        const result = await axios[method](url, data);
        console.log(result.status)
        if (![200,202,201,301,302].includes(result.status)) {
            throw new Error(`Could not retrieve data`);
        }

        return result.data.data || result.data;
    } catch (err) {
        console.log(err, url);

        throw err;
    }
};

const sleep = (sec) => new Promise((resolve) => setTimeout(resolve, sec * 1000));
const waitForState = async (waitFor, serviceFunction, id) => {
  let retry = 120;
  let state = '';
  while (state !== waitFor && retry > 0) {
    state = (await serviceFunction(id)).state;
    retry--;
    await sleep(1);
  }

  if (retry === 0) {
    throw new Error(`Maximum retries exceeded waiting for state ${waitFor}`);
  }
}

const getServiceByServiceNameAndStackId = async (stackId) => {
    const url = `/${PROJECT_ID}/services?name=${SERVICE_NAME}&stackId=${stackId}`;

    return makeCall(url, 'get', {});
};

const getServiceById = async (serviceId) => {
    const url = `/${PROJECT_ID}/services/${serviceId}`;

    return makeCall(url, 'get', {});
};

const getStack = async () => {
    const url = `/${PROJECT_ID}/stacks?name=${STACK_NAME}`;

    return makeCall(url, 'get', {});
};

const doUpgrade = async (serviceId, serviceData) => {
    const url = `/${PROJECT_ID}/service/${serviceId}?action=upgrade`;

    return makeCall(url, 'post', serviceData);
};

const finishUpgrade = async (serviceId) => {
    const url = `/${PROJECT_ID}/service/${serviceId}?action=finishupgrade`;

    return makeCall(url, 'post', {});
};

process.on('unhandledRejection', handleError);

(async () => {
    const stack = await getStack();

    if (!stack[0]) {
        throw new Error('Could not find stack name. Check the stack_name input. Deploy failed!');
    }
    const stackId = stack[0].id;
    const service = await getServiceByServiceNameAndStackId(stackId);

    if (!service[0]) {
        throw new Error('Could not find service name. Check the service_name input. Deploy failed!');
    }

    const { id, launchConfig } = service[0];
    launchConfig.imageUuid = `docker:${DOCKER_IMAGE}`;
    const body = {
        inServiceStrategy: {
            launchConfig
        }
    }

    await doUpgrade(id, body);
    console.log('Waiting for upgrade ...');
    await waitForState('upgraded', getServiceById, id);

    await finishUpgrade(id);
    console.log('Waiting for service starting ...');
    await waitForState('active', getServiceById, id);

    console.log('Service is running, upgrade successful');
})();