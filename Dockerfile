FROM node:10.15-slim

COPY package*.json /
COPY index.js /
COPY LICENSE.txt pipe.yml README.md /

RUN npm install

ENTRYPOINT ["node", "/index.js"]